const lists = require("./lists.json")
// console.log(lists);

let listed = (boardID, lists) => {
    
    let listFetchID = Object.keys(lists);
    
    let resultedList = listFetchID.reduce((acc, curr) =>{
        // console.log(acc);
        if(curr === boardID){
            acc.push(lists[curr])

        }
        return acc;

    }, [])

    return resultedList;
}

let mainFunc = (callback, boardID) => {

    setTimeout(()=>{
        console.log(callback(boardID, lists))
    }, 2000)
}

module.exports = {mainFunc, listed}


